## Setup
pod install

## Usage
Slider: zoom the different UILabels by setting new frames
SegmentControl: Control the number of labels displayed

Result: Framerate drops with increasing number of UILabels


##Goal
Find out reasons and possible tweaks to improve performance

