//
//  ViewController.swift
//  zoomer
//
//  Created by Chris on 17.02.18.
//  Copyright © 2018 thinkfield. All rights reserved.
//

import UIKit


/*
    Findings:
    1. Rendering UILabel is significantly quicker than UITextView
 */

class ViewController: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.contentView
    }
    
    
    

    let standardSize = CGSize(width: 50, height: 50)
    let standardSpacing = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
    
    @IBAction func resizeTo(_ sender: UISlider)
    {
        let scale = CGFloat(sender.value)
        for sw in self.contentView.subviews{
            sw.frame.size =  standardSize.applying(CGAffineTransform(scaleX: scale, y: scale))
        }
    }
    func frame(for index: IndexPath) -> CGRect{
        let x = standardSize.width * CGFloat(index.section) + (standardSpacing.left + standardSpacing.right) * CGFloat(index.section + 1)
        let y = standardSize.height * CGFloat(index.row) + (standardSpacing.top + standardSpacing.bottom) * CGFloat(index.row + 1)
        let origin = CGPoint(x: x, y: y)
        let size = standardSize
        return CGRect(origin: origin, size: size)
    }
    
    func addTextSettableView<ViewType: TextSettableView>(_ viewType: ViewType.Type, at path: IndexPath , viewInit: (ViewType) -> () ){
            let frame = self.frame(for: path)
            let text = "Lorem ipsum dolor sit thinkfieldLorem ipsum dolor sit thinkfieldLorem ipsum dolor sit thinkfieldLorem ipsum dolor sit thinkfieldLorem ipsum dolor sit thinkfieldLorem ipsum dolor sit thinkfieldLorem ipsum dolor sit thinkfield"
            var view = ViewType(frame: frame)
            viewInit(view)
            view.textValue = text
            contentView.addSubview(view)
        }
    
    
    let quantities = [10,50,100,200]
    
    @IBAction func didSelectLabelQuantity(_ sender: UISegmentedControl) {
        let index = sender.selectedSegmentIndex
        let quantity  = quantities.indices.contains(index) ? quantities[index] : 10
        addLabels(amount: quantity)
    }
    
    func addLabels( amount: Int){
        for subview in contentView.subviews{
            subview.removeFromSuperview()
        }
        let rowSize = 20
        
        if useTextView{
            addViews(of: UITextView.self, n: amount, rowSize: rowSize) { (tv) in
                tv.backgroundColor = .clear
            }
        }
        else{
            addViews(of: UILabel.self, n: amount, rowSize: rowSize) { (label) in
                label.numberOfLines = 0
            }
        }
    }
    
    func addViews<ViewType: TextSettableView>(of type: ViewType.Type, n: Int, rowSize: Int,viewInit: (ViewType) -> () ){
        
        var indices = [IndexPath]()
        
        while indices.count < n {
            guard let lastIndex = indices.last else{
                indices.append(IndexPath(row: 0, section: 0) )
                continue
            }
            let  row =  (lastIndex.section == rowSize - 1) ? lastIndex.row + 1 : lastIndex.row
            let section =  (lastIndex.section == rowSize - 1)  ? 0  : lastIndex.section + 1
                indices.append(IndexPath(row: row  , section: section) )
        }
        
        for i in indices {
            self.addTextSettableView(type, at: i, viewInit: viewInit)
        }
        
    }
    
    
    let useTextView = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addLabels(amount: 10)
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.scrollView.contentSize = CGSize(width: 10000, height: 10000)
        self.scrollView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

