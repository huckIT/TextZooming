//
//  TextSettable.swift
//  zoomer
//
//  Created by Chris on 17.02.18.
//  Copyright © 2018 thinkfield. All rights reserved.
//

import Foundation
import UIKit

protocol TextSettable{
    var textValue: String?  {get set}
}

typealias  TextSettableView = TextSettable & UIView

extension UITextView: TextSettable{
    var textValue: String? {
        get {
            return self.text
        }
        set {
            self.text = newValue
        }
    }
}

extension UILabel: TextSettable {
    var textValue: String? {
        get {
            return self.text
        }
        set {
            self.text = newValue
        }
    }
}
extension UITextField: TextSettable{
    var textValue: String? {
        get {
            return self.text
        }
        set {
            self.text = newValue
        }
    }
}
